package melomel.core
{
import melomel.core.uiClasses.Sandbox;
import melomel.core.uiClasses.TestTab;
import melomel.display.FlashDisplayObject;
import melomel.display.FlashDisplayObjectContainer;
import melomel.display.FlashStage;
import melomel.display.IDisplayObject;
import melomel.display.IDisplayObjectContainer;
import melomel.display.IStage;

import org.flexunit.Assert;
import org.flexunit.async.Async;
import org.fluint.uiImpersonation.UIImpersonator;

import spark.components.Button;
import spark.components.ComboBox;
import spark.components.TextInput;
import mx.collections.ArrayList;
import mx.controls.ComboBox;
import mx.controls.DataGrid;
import mx.controls.Tree;
import mx.containers.Panel;
import mx.core.FlexGlobals;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.display.NativeWindow;
import flash.display.NativeWindowInitOptions;
import flash.desktop.NativeApplication;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

public class UITest
{
	//---------------------------------------------------------------------
	//
	//  Setup
	//
	//---------------------------------------------------------------------

	private var sandbox:Sandbox;
    private var melomelType:IDisplayObjectContainer;
	private var window:NativeWindow;
	
	[Before(async)]
	public function setUp():void
	{
		sandbox = new Sandbox();
        melomelType = new FlashDisplayObjectContainer(sandbox);
		Async.proceedOnEvent(this, sandbox, FlexEvent.CREATION_COMPLETE, 500);

		UIImpersonator.addChild(sandbox);
	}
	
	[After]
	public function tearDown():void
	{
		UIImpersonator.removeChild(sandbox);
		sandbox = null;
		
		if(window) {
			window.close();
		}
	}
	
	
	//---------------------------------------------------------------------
	//
	//  Static methods
	//
	//---------------------------------------------------------------------

	//-----------------------------
	//  Find
	//-----------------------------

	[Test(timeout="1000")]
	public function shouldFindSingleComponentById():void
	{
		var components:Array = UI.findAll(Button, melomelType, {id:"button1"});
		Assert.assertEquals(1, components.length);
		Assert.assertEquals(sandbox.button1, components[0].base);
	}

	[Test(timeout="1000")]
	public function shouldFindMultipleComponents():void
	{
		var components:Array = UI.findAll(Button, melomelType);
		Assert.assertEquals(2, components.length);
		Assert.assertEquals(sandbox.button1, components[0].base);
		Assert.assertEquals(sandbox.button2, components[1].base);
	}

	[Test(timeout="1000")]
	public function shouldFindMultipleComponentsAsString():void
	{
		var components:Array = UI.findAll("spark.components.Button", melomelType);
		Assert.assertEquals(2, components.length);
		Assert.assertEquals(sandbox.button1, components[0].base);
		Assert.assertEquals(sandbox.button2, components[1].base);
	}

	[Test(timeout="1000")]
	public function shouldFindMultipleClasses():void
	{
		var components:Array = UI.findAll([Button, TextInput], melomelType);
		Assert.assertEquals(5, components.length);
		Assert.assertEquals(sandbox.button1, components[0].base);
		Assert.assertEquals(sandbox.textInput1, components[1].base);
		Assert.assertEquals(sandbox.button2, components[2].base);
		Assert.assertEquals(sandbox.barField, components[3].base);
		Assert.assertEquals(sandbox.tab0.textInput, components[4].base);
	}

	[Test(timeout="1000")]
	public function shouldFindMultipleClassesAsStrings():void
	{
		var components:Array = UI.findAll(["spark.components.Button", "spark.components.TextInput"], melomelType);
		Assert.assertEquals(5, components.length);
		Assert.assertEquals(sandbox.button1, components[0].base);
		Assert.assertEquals(sandbox.textInput1, components[1].base);
		Assert.assertEquals(sandbox.button2, components[2].base);
		Assert.assertEquals(sandbox.barField, components[3].base);
		Assert.assertEquals(sandbox.tab0.textInput, components[4].base);
	}

	[Test(timeout="1000")]
	public function shouldFindMultipleClassesAsStrings2():void
	{
		var components:Array = UI.findAll(["spark.components.Button", "no.such.class"], melomelType);
		Assert.assertEquals(2, components.length);
		Assert.assertEquals(sandbox.button1, components[0].base);
		Assert.assertEquals(sandbox.button2, components[1].base);
	}

	[Test(timeout="1000")]
	public function shouldFindOnlyVisibleTextInputs():void
	{
		var components:Array = UI.findAll(TextInput, melomelType, {id:'textInput'});
		Assert.assertEquals(1, components.length);
		Assert.assertEquals(sandbox.tab0.textInput, components[0].base);
	}

	[Test(timeout="1000")]
	public function shouldFindNoComponentsIfNoneMatch():void
	{
		var components:Array = UI.findAll(DataGrid, melomelType);
		Assert.assertEquals(0, components.length);
	}

	[Test(expects="melomel.errors.MelomelError")]
	public function shouldErrorWhenNoClassNameIsProvided():void
	{
		UI.findAll(null, melomelType);
	}

	[Test(timeout="1000")]
	public function shouldFindLabeledHaloComponent():void
	{
		var component:Object = UI.findLabeled("mx.controls.TextInput", "Foo Field", melomelType);
		Assert.assertEquals(sandbox.fooField, component.base);
	}

	[Test(timeout="1000")]
	public function shouldFindLabeledSparkComponent():void
	{
		var component:Object = UI.findLabeled("spark.components.TextInput", "Bar Field", melomelType);
		Assert.assertEquals(sandbox.barField, component.base);
	}

	[Test(timeout="1000")]
	public function shouldNotFindInvalidLabeledComponent():void
	{
		var component:Object = UI.findLabeled("mx.controls.TextInput", "Baz", melomelType);
		Assert.assertNull(component);
	}

	[Test(timeout="1000")]
	public function shouldFindLabeledLabel():void
	{
		var component:IDisplayObject = UI.findLabeled("mx.controls.Label", "First Name", melomelType);
		Assert.assertEquals(sandbox.firstNameLabel, component.base);
	}

	[Test(timeout="1000")]
	public function shouldFindTabsOnTabNavigator():void
	{
		var tabs:Array = UI.findAll("mx.controls.tabBarClasses.Tab", melomelType);
		Assert.assertEquals(3, tabs.length);
	}

	[Test(timeout="1000")]
	public function shouldFindLabeledFormItemControl():void
	{
		var component:IDisplayObject = UI.findLabeled("mx.controls.TextInput", "Lucky Number", melomelType);
		Assert.assertEquals(sandbox.formItemTextInput, component.base);
	}

	//-----------------------------
	//  Find
	//-----------------------------

	[Test(timeout="1000")]
	public function findChildComponent():void
	{
		var component:Object = UI.find(Button, melomelType, {label:"Click Me"});
		Assert.assertEquals(sandbox.button1, component.base);
	}

	[Test(timeout="1000")]
	public function findWithNoRootSpecified():void
	{
		Melomel.initialize();

		var button:Button = new Button();
		button.id = "testButton";
		FlexGlobals.topLevelApplication.addChild(button);
		var component:IDisplayObject = UI.find(Button, null, {id:"testButton"});
		FlexGlobals.topLevelApplication.removeChild(button);
		Assert.assertEquals(button, component.base);
	}

	[Test(timeout="1000")]
	public function findRootComponent():void
	{
		var component:IDisplayObject = UI.find(Sandbox, melomelType);
		Assert.assertEquals(sandbox, component.base);
	}

	[Test(timeout="1000")]
	public function findWithClassName():void
	{
		var component:IDisplayObject = UI.find("spark.components.Button", melomelType, {id:"button2"});
		Assert.assertEquals(sandbox.button2, component.base);
	}

	[Test(timeout="1000")]
	public function findWithMultipleProperties():void
	{
		var component:IDisplayObject = UI.find(Button, melomelType, {id:"button2", label:"Click Me"});
		Assert.assertEquals(sandbox.button2, component.base);
	}

	[Test(timeout="1000")]
	public function shouldFindPopUp():void
	{
		var panel:Panel = new Panel();
		panel.title = "Foo";
		PopUpManager.addPopUp(panel, FlexGlobals.topLevelApplication as DisplayObject);
		var component:IDisplayObject = UI.find(Panel, null, {title:"Foo"});
		PopUpManager.removePopUp(panel);
		Assert.assertEquals(panel, component.base);
	}

	[Test(timeout="1000")]
	public function shouldFindInActiveWindow():void
	{
		var sprite:Sprite = new Sprite();
		sprite.name = "foo";
		window = new NativeWindow(new NativeWindowInitOptions());
		window.stage.addChild(sprite);
		window.activate();
		
		var component:IDisplayObject = UI.find(Sprite, null, {name:"foo"});
		Assert.assertEquals(sprite, component.base);
	}


	//-----------------------------
	//  Mouse Interactions
	//-----------------------------

	[Ignore]
	[Test(async)]
	public function clickShouldOccurInMiddleOfComponent():void
	{
		Async.handleEvent(this, sandbox.button1, MouseEvent.CLICK, function(event:MouseEvent,...args):void{
			Assert.assertEquals(50, event.localX);
			Assert.assertEquals(10, event.localY);
		});
		UI.click(new FlashDisplayObject(sandbox.button1));
	}

	[Test(async,expects="melomel.errors.MelomelError")]
	public function clickWhenDisabledThrowsError():void
	{
		sandbox.button1.enabled = false;
		UI.click(new FlashDisplayObject(sandbox.button1));
	}

	[Test(async,expects="melomel.errors.MelomelError")]
	public function clickWhenMouseDisabledThrowsError():void
	{
		sandbox.button1.mouseEnabled = false;
		UI.click(new FlashDisplayObject(sandbox.button1));
	}

	[Ignore]
	[Test(async)]
	public function doubleClick():void
	{
		Async.proceedOnEvent(this, sandbox.button1, MouseEvent.CLICK);
		Async.proceedOnEvent(this, sandbox.button1, MouseEvent.CLICK);
		Async.proceedOnEvent(this, sandbox.button1, MouseEvent.DOUBLE_CLICK);
		UI.doubleClick(new FlashDisplayObject(sandbox.button1));
	}

	[Test(async,expects="melomel.errors.MelomelError")]
	public function clickWhenDoubleClickDisabledThrowsError():void
	{
		sandbox.button1.doubleClickEnabled = false;
		UI.doubleClick(new FlashDisplayObject(sandbox.button1));
	}


	//-----------------------------
	//  Keyboard Interactions
	//-----------------------------

	[Test(async)]
	public function keyDownShouldFireKeyDownEvent():void
	{
		Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN,20000);
		UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
	}

	[Test(async)]
	public function keyDownWithLowerCaseLetter():void
	{
		Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
			Assert.assertEquals(97, event.charCode);
			Assert.assertEquals(65, event.keyCode);
		});
		UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), "a");
	}
	
	[Test(async)]
	public function keyDownWithUpperCaseLetter():void
	{
		Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
			Assert.assertEquals(65, event.charCode);
			Assert.assertEquals(65, event.keyCode);
		});
		UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
	}
	
	[Test(async)]
	public function keyDownWithKeyCodeValue():void
	{
		Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
			Assert.assertEquals(65, event.charCode);
			Assert.assertEquals(65, event.keyCode);
		});
		UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), Keyboard.A);
	}

	[Test(async)]
	public function keyDownWithControlCharacterShouldNotHaveACharCode():void
	{
		Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
			Assert.assertEquals(0, event.charCode);
			Assert.assertEquals(13, event.keyCode);
		});
		UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), Keyboard.ENTER);
	}
	
	[Test(async)]
	public function keyUpShouldFireKeyUpEvent():void
	{
		Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_UP);
		UI.keyUp(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
	}
	
	[Test(async)]
	public function keyPressShouldFireKeyUpAndKeyDownEvent():void
	{
		Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN);
		Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_UP);
		UI.keyPress(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
	}
	
	
	//-----------------------------
	//  Data Control Interaction
	//-----------------------------

	[Test]
	public function shouldGenerateLabelsFromHaloComboBox():void
	{
		var combo:mx.controls.ComboBox = new mx.controls.ComboBox();
		combo.labelField = "label";
		var data:Array = [{label:"foo"}, {label:"bar"}];
		var labels:Array = UI.itemsToLabels(combo, data);
		Assert.assertEquals(2, labels.length);
		Assert.assertEquals("foo", labels[0]);
		Assert.assertEquals("bar", labels[1]);
	}

	[Test]
	public function shouldGenerateLabelsFromSparkComboBox():void
	{
		var combo:spark.components.ComboBox = new spark.components.ComboBox();
		combo.labelField = "label";
		var data:ArrayList = new ArrayList([{label:"foo"}, {label:"bar"}]);
		var labels:Array = UI.itemsToLabels(combo, data);
		Assert.assertEquals(2, labels.length);
		Assert.assertEquals("foo", labels[0]);
		Assert.assertEquals("bar", labels[1]);
	}


	//-----------------------------
	//  Tree
	//-----------------------------

	[Test]
	public function shouldFindTreeItemByLabel():void
	{
		var tree:Tree = new mx.controls.Tree();
		tree.labelField = "label";
		var bazObject:Object = {label:"baz"};
		var data:Object = {
			label:"root",
			children:[
				{label:"foo"},
				{
					label:"bar",
					children: [
						bazObject,
						{label:"bat"}
					]
				}
			]
		};
		tree.dataProvider = data;
		
		var item:Object = UI.findTreeItemByLabel(tree, "baz");
		Assert.assertEquals(bazObject, item);
	}
}
}
