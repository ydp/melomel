/**
 * Created by njackson on 10/01/2014.
 */
package melomel.core.uiClasses {

import starling.display.Button;
import starling.display.Sprite;
import starling.text.TextField;
import starling.textures.Texture;

public class SimpleLayoutContainer extends Sprite {
    private var _textField1:TextField;
    private var _button1:Button;

    public function SimpleLayoutContainer() {

        _textField1 = new TextField(50, 50, "text");
        _textField1.name = "sltextfield1";
        addChild(_textField1);

        _button1 = new Button(Texture.fromColor(50, 50, 0xFF0000), "slbutton1", null);
        _button1.name = "slbutton1";
        _button1.visible=true;
        addChild(_button1);

    }
}
}
