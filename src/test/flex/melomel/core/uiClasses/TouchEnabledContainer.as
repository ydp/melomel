/**
 * Created by njackson on 14/01/2014.
 */
package melomel.core.uiClasses {
import org.gestouch.events.GestureEvent;
import org.gestouch.gestures.TapGesture;

import starling.display.Image;

import starling.display.Sprite;
import starling.events.Event;
import starling.textures.Texture;

public class TouchEnabledContainer extends Sprite {

    public function TouchEnabledContainer() {
        var texture:Texture = Texture.fromColor(50, 50, 0x00FF00);
        var image:Image = new Image(texture);
        addChild(image);

        var tap:TapGesture = new TapGesture( this );
        tap.addEventListener( GestureEvent.GESTURE_RECOGNIZED, onTap, false, 0, true );
    }

    private function onTap(event:GestureEvent):void {
        dispatchEvent(new Event(starling.events.Event.TRIGGERED,true));
    }

}
}
