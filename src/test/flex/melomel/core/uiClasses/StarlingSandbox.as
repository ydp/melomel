/**
 * Created by zan on 06/01/2014.
 */
package melomel.core.uiClasses
{
import flash.display.NativeWindow;
import flash.display.NativeWindowInitOptions;
import flash.display.NativeWindowSystemChrome;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.MouseEvent;

import mx.core.UIComponent;

import org.gestouch.core.Gestouch;
import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
import org.gestouch.extensions.starling.StarlingTouchHitTester;
import org.gestouch.input.NativeInputAdapter;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Stage;
import starling.events.Event;

[SWF(width="400", height="300", frameRate="60", backgroundColor="#FFFFFF")]
public class StarlingSandbox extends UIComponent
{

    private var _starling:Starling;
    public static const COMPLETE_EVENT:String = "COMPLETE_EVENT";

    public function get starlingstage():Stage {
        return _starling.stage;
    }

    public function StarlingSandbox(){
    }

    public function Startup():void
    {
        //don't delete that, we don't know why but don't, trust us.
        var options:NativeWindowInitOptions;
        options = new NativeWindowInitOptions();
        options.renderMode = "direct";
        options.transparent = true;
        options.systemChrome = NativeWindowSystemChrome.NONE;
        options.renderMode = "direct";
        var nativeWindow:NativeWindow;
        nativeWindow = new NativeWindow( options );
        nativeWindow.activate();

        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;

        _starling = new Starling(StarlingSandboxGame, stage);

        Gestouch.inputAdapter = new NativeInputAdapter( stage,false,true);
        Gestouch.addDisplayListAdapter( DisplayObject, new StarlingDisplayListAdapter() );
        Gestouch.addTouchHitTester( new StarlingTouchHitTester( _starling ), -1 );

        _starling.start();
        _starling.addEventListener(starling.events.Event.ROOT_CREATED, onStarlingRootCreated);

    }

    private function onClick(event:starling.events.Event):void {
        dispatchEvent(new flash.events.Event(MouseEvent.CLICK));
    }

    private function onStarlingRootCreated(event:Object):void
    {
        starlingstage.addEventListener(starling.events.Event.TRIGGERED,onClick);
        removeEventListener(starling.events.Event.ADDED_TO_STAGE, onStarlingRootCreated);
        dispatchEvent(new flash.events.Event(flash.events.Event.ADDED_TO_STAGE));
    }

}
}
