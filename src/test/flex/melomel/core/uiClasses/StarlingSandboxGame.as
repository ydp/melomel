/**
 * Created by zan on 06/01/2014.
 */
package melomel.core.uiClasses
{

import starling.display.Button;
import starling.display.Sprite;
import starling.text.TextField;
import starling.textures.Texture;

public class StarlingSandboxGame extends Sprite
{
    private var _button1:Button;
    private var _button2:Button;
    private var _textField1:TextField;
    private var _simpleLayout:SimpleLayoutContainer;
    private var _touchEnabledContainer:TouchEnabledContainer;

    public function get button1():Button
    {
        return _button1;
    }

    public function get button2():Button{
        return _button2;
    }

    public function get textField1():TextField{
        return _textField1;
    }

    public function get simpleLayout():SimpleLayoutContainer {
        return _simpleLayout;
    }

    public function get touchEnabledContainer():TouchEnabledContainer {
        return _touchEnabledContainer;
    }

    public function StarlingSandboxGame()
    {
        _button1 = new Button(Texture.fromColor(50, 50, 0xFF0000), "button1", null);
        _button1.name = "button1";
        _button1.visible=true;
        addChild(button1);


        _button2 = new Button(Texture.fromColor(50, 50, 0x00FF00), "button2", null);
        _button2.name = "button2";
        _button2.text = "Click Me";
        _button2.visible=true;
        addChild(button2);

        _textField1 = new TextField(50, 50, "text");
        _textField1.name = "textfield1";
        addChild(textField1);

        _simpleLayout = new SimpleLayoutContainer();
        _simpleLayout.name = "simplelayout";
        addChild(_simpleLayout);

        _touchEnabledContainer = new TouchEnabledContainer();
        _touchEnabledContainer.name = "touchenabledcontainer";
        addChild(_touchEnabledContainer);
    }

}
}
