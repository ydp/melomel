/**
 * Created by zan on 06/01/2014.
 */
package melomel.core
{
import flash.display.NativeWindow;
import flash.events.Event;
import flash.events.MouseEvent;

import melomel.core.uiClasses.StarlingSandbox;
import melomel.core.uiClasses.StarlingSandboxGame;

import melomel.display.IDisplayObject;
import melomel.display.IDisplayObjectContainer;
import melomel.display.StarlingDisplayObject;
import melomel.display.StarlingDisplayObjectContainer;

import org.flexunit.Assert;
import org.flexunit.async.Async;

import org.fluint.uiImpersonation.UIImpersonator;

import spark.components.DataGrid;

import starling.display.Button;
import starling.text.TextField;

public class StarlingUITest
{
    //---------------------------------------------------------------------
    //
    //  Setup
    //
    //---------------------------------------------------------------------

    private  var sandbox:StarlingSandboxGame;
    private  var melomelType:IDisplayObjectContainer;
    private  var window:NativeWindow;
    private  var _starlingSandbox:StarlingSandbox ;

    [Before(async, ui, order=1)]
    public function setUp():void{
        _starlingSandbox = new StarlingSandbox();
        UIImpersonator.addChild(_starlingSandbox);
        Async.proceedOnEvent(this, _starlingSandbox, flash.events.Event.ADDED_TO_STAGE, 5000);
        _starlingSandbox.Startup();
        melomelType = new StarlingDisplayObjectContainer(_starlingSandbox.starlingstage);
    }

    [Before(async, ui, order=2)]
    public function setUpWhenStarlingInitiated():void
    {
        var sandboxContainer = melomelType.getChildAt(0);
        sandbox = sandboxContainer.base as StarlingSandboxGame;
    }


    [After(async, ui)]
    public function tearDown():void
    {
        UIImpersonator.removeChild(_starlingSandbox);
        sandbox = null;
        melomelType = null;

        if(window) {
            window.close();
        }
    }

    //---------------------------------------------------------------------
    //
    //  Static methods
    //
    //---------------------------------------------------------------------

    //-----------------------------
    //  Find
    //-----------------------------

    [Test(async, ui)]
    public function shouldFindSingleComponentByName():void
    {
        var components:Array = UI.findAll(Button, melomelType, {name:"button1"});
        Assert.assertEquals(1, components.length);
        Assert.assertEquals(sandbox.button1, components[0].base);
    }


    [Test(async, ui)]
    public function shouldFindMultipleComponents():void
    {
        var components:Array = UI.findAll(Button, melomelType);
        Assert.assertEquals(3, components.length);
        Assert.assertEquals(sandbox.button1, components[0].base);
        Assert.assertEquals(sandbox.button2, components[1].base);
    }

    [Test(async, timeout="1000")]
    public function shouldFindMultipleComponentsAsString():void
    {
        var components:Array = UI.findAll("starling.display.Button", melomelType);
        Assert.assertEquals(3, components.length);
        Assert.assertEquals(sandbox.button1, components[0].base);
        Assert.assertEquals(sandbox.button2, components[1].base);
    }

    [Test(async, timeout="1000")]
    public function shouldFindMultipleClasses():void
    {
        var components:Array = UI.findAll([Button,TextField], melomelType, null, 0, 2);
        Assert.assertEquals(3, components.length);
        Assert.assertEquals(sandbox.button1, components[0].base);
        Assert.assertEquals(sandbox.textField1, components[2].base);
        Assert.assertEquals(sandbox.button2, components[1].base);
    }
    [Test(timeout="1000")]
    public function shouldFindMultipleClassesAsStrings():void
    {
        var components:Array = UI.findAll(["starling.display.Button", "starling.text.TextField"], melomelType, null, 0, 2);
        Assert.assertEquals(3, components.length);
        Assert.assertEquals(sandbox.button1, components[0].base);
        Assert.assertEquals(sandbox.textField1, components[2].base);
        Assert.assertEquals(sandbox.button2, components[1].base);
    }
//
    [Test(timeout="1000")]
    public function shouldFindMultipleClassesAsStrings2():void
    {
        var components:Array = UI.findAll(["starling.display.Button", "no.such.class"], melomelType);
        Assert.assertEquals(3, components.length);
        Assert.assertEquals(sandbox.button1, components[0].base);
        Assert.assertEquals(sandbox.button2, components[1].base);
    }

    [Test(timeout="1000")]
    public function shouldFindNoComponentsIfNoneMatch():void
    {
        var components:Array = UI.findAll(DataGrid, melomelType);
        Assert.assertEquals(0, components.length);
    }

    [Test(expects="melomel.errors.MelomelError")]
    public function shouldErrorWhenNoClassNameIsProvided():void
    {
        UI.findAll(null, melomelType);
    }


//    //-----------------------------
//    //  Find
//    //-----------------------------
//
    [Test(timeout="1000")]
    public function findChildComponent():void
    {
        var component:Object = UI.find(Button, melomelType, {text:"Click Me"});
        Assert.assertEquals(sandbox.button2, component.base);
    }

    [Test(timeout="1000")]
    public function findRootComponent():void
    {
        var component:IDisplayObject = UI.find(StarlingSandboxGame, melomelType);
        Assert.assertEquals(sandbox, component.base);
    }

    [Test(timeout="1000")]
    public function findWithClassName():void
    {
        var component:IDisplayObject = UI.find("melomel.core.uiClasses.SimpleLayoutContainer", melomelType, {name:"simplelayout"});
        Assert.assertEquals(sandbox.simpleLayout, component.base);
    }

    [Test(timeout="1000")]
    public function findWithMultipleProperties():void
    {
        var component:IDisplayObject = UI.find(Button, melomelType, {name:"button2", text:"Click Me"});
        Assert.assertEquals(sandbox.button2, component.base);
    }
//
//    [Test(timeout="1000")]
//    public function shouldFindPopUp():void
//    {
//        var panel:Panel = new Panel();
//        panel.title = "Foo";
//        PopUpManager.addPopUp(panel, FlexGlobals.topLevelApplication as DisplayObject);
//        var component:IDisplayObject = UI.find(Panel, null, {title:"Foo"});
//        PopUpManager.removePopUp(panel);
//        Assert.assertEquals(panel, component.base);
//    }
//
//    [Test(timeout="1000")]
//    public function shouldFindInActiveWindow():void
//    {
//        var sprite:Sprite = new Sprite();
//        sprite.name = "foo";
//        window = new NativeWindow(new NativeWindowInitOptions());
//        window.stage.addChild(sprite);
//        window.activate();
//
//        var component:IDisplayObject = UI.find(Sprite, null, {name:"foo"});
//        Assert.assertEquals(sprite, component.base);
//    }
//
//
//    //-----------------------------
//    //  Mouse Interactions
//    //-----------------------------
//
//    [Ignore]
//    [Test(async)]
//    public function clickShouldOccurInMiddleOfComponent():void
//    {
//        Async.handleEvent(this, sandbox.button1, MouseEvent.CLICK, function(event:MouseEvent,...args):void{
//            Assert.assertEquals(50, event.localX);
//            Assert.assertEquals(10, event.localY);
//        });
//        UI.click(new FlashDisplayObject(sandbox.button1));
//    }
//
    [Test(async,expects="melomel.errors.MelomelError")]
    public function clickWhenDisabledThrowsError():void
    {
        sandbox.button1.enabled = false;
        UI.click(new StarlingDisplayObject(sandbox.button1));
    }

    [Test(async,expects="melomel.errors.MelomelError")]
    public function clickWhenMouseDisabledThrowsError():void
    {
        sandbox.button1.touchable = false;
        UI.click(new StarlingDisplayObject(sandbox.button1));
    }

    [Test(async)]
    public function clickButton():void
    {
        Async.proceedOnEvent(this, _starlingSandbox, MouseEvent.CLICK);
        UI.click(new StarlingDisplayObject(sandbox.button1));
    }

    [Test(async)]
    public function tapGestouchEnabledObject():void
    {
        Async.proceedOnEvent(this, _starlingSandbox, MouseEvent.CLICK);
        UI.touch(new StarlingDisplayObject(sandbox.touchEnabledContainer));
    }
//
//    [Test(async,expects="melomel.errors.MelomelError")]
//    public function clickWhenDoubleClickDisabledThrowsError():void
//    {
//        sandbox.button1.doubleClickEnabled = false;
//        UI.doubleClick(new FlashDisplayObject(sandbox.button1));
//    }
//
//
//    //-----------------------------
//    //  Keyboard Interactions
//    //-----------------------------
//
//    [Test(async)]
//    public function keyDownShouldFireKeyDownEvent():void
//    {
//        Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN,20000);
//        UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
//    }
//
//    [Test(async)]
//    public function keyDownWithLowerCaseLetter():void
//    {
//        Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
//            Assert.assertEquals(97, event.charCode);
//            Assert.assertEquals(65, event.keyCode);
//        });
//        UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), "a");
//    }
//
//    [Test(async)]
//    public function keyDownWithUpperCaseLetter():void
//    {
//        Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
//            Assert.assertEquals(65, event.charCode);
//            Assert.assertEquals(65, event.keyCode);
//        });
//        UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
//    }
//
//    [Test(async)]
//    public function keyDownWithKeyCodeValue():void
//    {
//        Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
//            Assert.assertEquals(65, event.charCode);
//            Assert.assertEquals(65, event.keyCode);
//        });
//        UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), Keyboard.A);
//    }
//
//    [Test(async)]
//    public function keyDownWithControlCharacterShouldNotHaveACharCode():void
//    {
//        Async.handleEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent,...args):void{
//            Assert.assertEquals(0, event.charCode);
//            Assert.assertEquals(13, event.keyCode);
//        });
//        UI.keyDown(new FlashDisplayObjectContainer(sandbox.textInput1), Keyboard.ENTER);
//    }
//
//    [Test(async)]
//    public function keyUpShouldFireKeyUpEvent():void
//    {
//        Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_UP);
//        UI.keyUp(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
//    }
//
//    [Test(async)]
//    public function keyPressShouldFireKeyUpAndKeyDownEvent():void
//    {
//        Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_DOWN);
//        Async.proceedOnEvent(this, sandbox.textInput1, KeyboardEvent.KEY_UP);
//        UI.keyPress(new FlashDisplayObjectContainer(sandbox.textInput1), "A");
//    }
//
}
}
