package melomel.display 
{
	import flash.events.IEventDispatcher;

    public interface IStage 
	{
        function removeEventListener(event:String, func:Function):*;
        function addEventListener(event:String, func:Function):*;
    }
}
