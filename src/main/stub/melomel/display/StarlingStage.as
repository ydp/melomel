package melomel.display 
{
	import flash.events.Event;

	import starling.display.Stage;

	public class StarlingStage implements IStage 
	{
		public function StarlingStage(stage:Stage) { }

		public function removeEventListener(event:String, func:Function) { }

		public function addEventListener(event:String, func:Function) { }
	}
}

