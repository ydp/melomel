/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author Ben Johnson
 */
package melomel.core
{

import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.geom.Point;
import flash.text.TextField;
import flash.utils.Timer;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;
import flash.utils.setTimeout;

import melomel.display.FlashStage;
import melomel.display.IDisplayObject;
import melomel.display.IDisplayObjectContainer;
import melomel.errors.MelomelError;

import spark.components.TextInput;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Stage;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

/**
 *	This class provides user interface related utility methods for finding
 *	display objects and appropriately interacting with them.
 */
public class UI
{
	//--------------------------------------------------------------------------
	//
	//	Static Properties
	//
	//--------------------------------------------------------------------------

	/**
	 *	A list of components whose rawChildren should be searched. This
	 *	defaults to the FormItem and Panel but other classes can
	 *	be added manually.
	 */



	//--------------------------------------------------------------------------
	//
	//	Static methods
	//
	//--------------------------------------------------------------------------

	//---------------------------------
	//	Initialize
	//---------------------------------

	/**
	 *	Initializes the UI class. This is normally done automatically but can
	 *	be called manually if rawChildrenClasses needs to be altered.
	 */
	static public function initialize():void
	{
		// Setup default rawChildren classes in UI

	}


	//---------------------------------
	//	Search
	//---------------------------------

	/**
	 *	A running count of matched objects within findAll(). This is not thread
	 *	safe but luckily Flash only has one thread.
	 */
	static private var count:uint = 0;
	static private var _pointArray:Array = new Array();
	static private var _removeTimer:Timer = new Timer(2000);
	/**
	 *	Recursively searches the display hiearchy to find a display object.
	 *
	 *	@param clazz       The class to match.
	 *	@param root        The root display object to start from.
	 *	@param properties  A hash of property values to match.
	 *  @param limit       Number of children to match.
	 *  @param levels      Levels of hierarchy below the root object to check
	 *
	 *	@return            A list of display objects matching the class and
	 *	                   properties specified in the criteria.
	 */



	static public function findAll(clazz:Object, root:IDisplayObject,
								   properties:Object=null, limit:uint=0, levels:int=-1):Array
	{
		// Attempt to default root if not specified
		if(!root) {
			// If running AIR, use the active window's stage
			var nativeApplicationClass:Class = Type.getClass("flash.desktop.NativeApplication");
			if(nativeApplicationClass && nativeApplicationClass.nativeApplication.activeWindow) {
				root = new FlashStage(nativeApplicationClass.nativeApplication.activeWindow.stage);
			}
			// Otherwise use the stage
			else {
				root = Melomel.stage;
			}
		}

		// Verify class is not null
		if(!clazz) {
			throw new MelomelError("A class name or reference is required");
		}
		// Find root if not passed in
		if(!root) {
			throw new MelomelError("The root display object is required");
		}

		// Convert clazz into an Array of classes.
		if(!(clazz is Array)) {
			clazz = [clazz];
		}

		// Convert class name to class reference, if necessary
		for(var i:int=0; i<clazz.length; i++) {
			if(clazz[i] is String) {
				try {
					clazz[i] = getDefinitionByName(clazz[i] as String);
				}
				catch(e:Error) {
					clazz.splice(i--, 1);
				}
			}
		}

		// Throw error if none of the classes passed in were found.
		if(clazz.length == 0) {
			throw new MelomelError("Classes could not be found");
		}

		count = 0;
		return _findAll(clazz as Array, root, properties, limit, levels);
	}

	static private function _findAll(classes:Array, root:IDisplayObject,
									 properties:Object, limit:uint=0, levels:uint=0):Array
	{
		// Match root object first
		var objects:Array = new Array();

		// Attempt to match all classes
		var match:Boolean;
		for each(var clazz:Class in classes) {
			match = false;

			if(root.base is clazz) {
				match = true;

				if(properties) {
					// Match display object on properties
					for(var propName:String in properties) {
						if(!root.hasOwnProperty(propName) ||
								root.getValueForProperty(propName) != properties[propName])
						{
							match = false;
							break;
						}
					}
				}
			}

			if(match) {
				break;
			}
		}

		// If class and properties match then add to list of components
		if(match && isVisible(root)) {
			objects.push(root);

			if(limit > 0 && ++count >= limit) {
				return objects;
			}
		}

		// Recursively search over children
		if(root is IDisplayObjectContainer && levels != 0) {

			var obj:IDisplayObjectContainer = root as IDisplayObjectContainer;

			var numChildren:int = obj.numChildren;
			for(var i:int=0; i<numChildren; i++) {
				var child:IDisplayObject = obj.getChildAt(i);

				// If matching descendants are found then append to list
				var arr:Array = _findAll(classes, child, properties, limit, levels-1);
				if(arr.length) {
					objects = objects.concat(arr);
				}

				// Exit if at limit.
				if(limit > 0 && count >= limit) {
					break;
				}
			}
		}

		// Return list of objects
		return objects;
	}

	/**
	 *	Recursively searches the display hiearchy to find a display object.
	 *
	 *	@param clazz       The class to match.
	 *	@param root        The root display object to start from.
	 *	@param properties  A hash of property values to match.
	 *
	 *	@return            A display object matching the class and properties
	 *	                   specified in the criteria.
	 */
	static public function find(clazz:Object, root:IDisplayObject,
								properties:Object=null):*
	{
		var objects:Array = findAll(clazz, root, properties, 1);
		return objects.shift();
	}


	/**
	 *	Attempts to find a component based on its position relative to a label.
	 *	This method starts by finding the label, then going up the parent
	 *	hierarchy and then recursively searching the parent for the first
	 *	component matching a given class.
	 *
	 *	@param clazz       The class to match.
	 *	@param labelText   The label text to match.
	 *	@param root        The root display object to start from.
	 *	@param properties  A hash of property values to match.
	 *
	 *	@return            The display object that is labeled by another
	 *                     display object.
	 */
	static public function findLabeled(clazz:Object, labelText:String,
									   root:IDisplayObject,
									   properties:Object=null):*
	{
		// First find label
		var label:Object = find(['mx.controls.Label', 'spark.components.Label', 'spark.components.FormItem'], root, {text:labelText});
		if (label == null)
			label = find(['mx.containers.FormItem'], root, {label:labelText});

		// If found, recursively search parent for component
		if(label && label.parent) {
			var components:Array = findAll(clazz, label.parent, properties);
			var labelInArray:IDisplayObject;
			// find the item and remove it, the hash of the item in the array will be different to the original item as we are
			// wrapping items in a container therefore we can not directly use indexOf
			components.forEach(function (element:IDisplayObject):void {
				if(element.base == label.base)
					labelInArray = element;
			});
			if(labelInArray) {
				components.splice(components.indexOf(labelInArray), 1);
			}
			return components.shift();
		}

		// If no label found, return null.
		return null;
	}

	/**
	 *	Checks if a display object is visible and all of its parents are
	 *	visible.
	 *
	 *	@param object  The display object to verify.
	 *
	 *	@return        True if all parents and object are visible. Otherwise,
	 *                 returns false.
	 */
	static public function isVisible(object:IDisplayObject):Boolean
	{
		// If we have no display object, return false
		if(object == null) {
			return false;
		}

		// Iterate up the parent hierarchy looking for invisible parents
		while(object != null) {
			// If we find an invisible parent, return false.
			if(!object.visible) {
				return false;
			}

			object = object.parent;
		}

		// If no parents were invisible, return true.
		return true;
	}


	//---------------------------------
	//	UI interactions
	//---------------------------------

	/**
	 *	Generalizes the interaction with a component.
	 *
	 *	@param event       The event.
	 *	@param component   The component to interact with.
	 *	@param properties  A hash of properties to set on the event object.
	 *	@param flags       A list of flags determining the behavior.
	 */
	static private function interact(event:Event, component:IDisplayObject,
									 properties:Object):void
	{
		// Default properties to an empty hash
		if(properties == null) {
			properties = {};
		}

		// If we are using a Flex component, make sure it is enabled
		if(!component.enabled) {
			throw new MelomelError("Flex component is disabled");
		}

		// Validate mouse-specific actions
		if(event is MouseEvent) {
			// Component must be mouse enabled
			if(!component.mouseEnabled) {
				throw new MelomelError("Mouse events are not enabled on component");
			}

			// Center the click
			if(!properties.localX) {
				properties.localX = Math.floor(component.width/2);
			}
			if(!properties.localY) {
				properties.localY = Math.floor(component.height/2);
			}
		}

		// Validate keyboard-specific actions
		if(event is KeyboardEvent) {
			var char:Object = properties["char"];
			delete properties["char"];

			// Verify that a key is specified to be pressed
			if(char == null || char == "") {
				throw new MelomelError("A keyboard key is required");
			}
			// Verify that only one character is specified
			if(char is String && char.length > 1) {
				throw new MelomelError("Only one keyboard key can be pressed at a time");
			}

			// Convert to keyCode & charCode
			if(char is String) {
				properties.charCode = char.charCodeAt(0);
				properties.keyCode  = char.toUpperCase().charCodeAt(0);

				// Automatically set the shift key for uppercase characters
				if(char.search(/[A-Z]/) != -1) {
					properties.shiftKey = true;
				}
			}
			else if(char is int) {
				if(String.fromCharCode(char).toUpperCase().search(/[0-9A-Z]/) != -1) {
					properties.charCode = String.fromCharCode(char).toUpperCase().charCodeAt(0);
				}
				else {
					properties.charCode = 0;
				}
				properties.keyCode  = char;
			}
			else {
				throw new MelomelError("Keyboard character must be a string or a key code value");
			}

			// Find UITextField inside text components
			component = find([TextField, TextInput], component);
		}

		// Copy properties to event
		for(var propName:String in properties) {
			event[propName] = properties[propName];
		}

		component.dispatchEvent(event);
	}

	static private function interactTouch(event:MouseEvent, component:IDisplayObject, properties:Object):void
	{
		trace("Touch Start");

		// Default properties to an empty hash
		if(properties == null) {
			properties = {};
		}

		// If we are using a Flex component, make sure it is enabled
		if(!component.enabled) {
			throw new MelomelError("Flex component is disabled");
		}

		// Validate mouse-specific actions
		if(event is MouseEvent) {
			// Component must be mouse enabled
			if(!component.mouseEnabled) {
				throw new MelomelError("Mouse events are not enabled on component");
			}

			// Center the click
			if(!properties.localX) {
				properties.localX = Math.floor(component.width/2);
			}
			if(!properties.localY) {
				properties.localY = Math.floor(component.height/2);
			}

			// convert the coord to global
			var local:Point = new Point(properties.localX, properties.localY);
			var global:Point = new Point();
			component.base.localToGlobal(local,global);
			properties.localX = global.x;
			properties.localY = global.y;
		}

		// Copy properties to event
		for(var propName:String in properties) {
			event[propName] = properties[propName];
		}

		// find the stage
		var stage:Stage = findStage(component)

		Starling.current.nativeStage.dispatchEvent(event);
	}

	static private function interactKeyboard(event:Event, component:IDisplayObject,
											 properties:Object):void
	{
		// Default properties to an empty hash
		if(properties == null) {
			properties = {};
		}

		// If we are using a Flex component, make sure it is enabled
		if(!component.enabled) {
			throw new MelomelError("Flex component is disabled");
		}


		// Validate keyboard-specific actions
		if(event is KeyboardEvent) {
			var char:Object = properties["char"];
			delete properties["char"];

			// Verify that a key is specified to be pressed
			if(char == null || char == "") {
				throw new MelomelError("A keyboard key is required");
			}
			// Verify that only one character is specified
			if(char is String && char.length > 1) {
				throw new MelomelError("Only one keyboard key can be pressed at a time");
			}

			// Convert to keyCode & charCode
			if(char is String) {
				properties.charCode = char.charCodeAt(0);
				properties.keyCode  = char.toUpperCase().charCodeAt(0);

				// Automatically set the shift key for uppercase characters
				if(char.search(/[A-Z]/) != -1) {
					properties.shiftKey = true;
				}
			}
			else if(char is int) {
				if(String.fromCharCode(char).toUpperCase().search(/[0-9A-Z]/) != -1) {
					properties.charCode = String.fromCharCode(char).toUpperCase().charCodeAt(0);
				}
				else {
					properties.charCode = 0;
				}
				properties.keyCode  = char;
			}
			else {
				throw new MelomelError("Keyboard character must be a string or a key code value");
			}

			// Find UITextField inside text components
			if(getQualifiedClassName(component.base) != "feathers.controls::TextInput" && getQualifiedClassName(component.base) != "eu.ydp.view.controls.text::TextInput") {
				component = find([TextField, TextInput], component);
			}

		}

		// Copy properties to event
		for(var propName:String in properties) {
			event[propName] = properties[propName];
		}


		if(getQualifiedClassName(component.base) == "feathers.controls::TextInput") {
			component.base.dispatchEvent(new starling.events.KeyboardEvent(((event['type'] == 'keyUp')?starling.events.KeyboardEvent.KEY_UP:starling.events.KeyboardEvent.KEY_DOWN), properties.charCode, properties.keyCode, 0, false, false, properties.shiftKey));
		} else if(getQualifiedClassName(component.base) == "eu.ydp.view.controls.text::TextInput") {
			component.base.textEditorInstance.dispatchEvent(new starling.events.KeyboardEvent(((event['type'] == 'keyUp')?starling.events.KeyboardEvent.KEY_UP:starling.events.KeyboardEvent.KEY_DOWN), properties.charCode, properties.keyCode, 0, false, false, properties.shiftKey));
		} else {
			component.dispatchEvent(event);
		}
	}

	private static function findStage(component:IDisplayObject):Stage {
		if (component.base is Stage)
			return component.base as Stage;
		else
			return findStage(component.parent);
	}

	/**
	 *	Imitates a click on a component.
	 *
	 *	@param component   The component to click.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function click(component:IDisplayObject,
								 properties:Object=null):void
	{
		//mouseDown(component, properties);
		//mouseUp(component, properties);

		interact(new MouseEvent(MouseEvent.CLICK), component, properties)
	}

	static public function longTouchClick(component:IDisplayObject, properties:Object=null):void
	{
		if(properties == null) {
			properties = {};
		}

		if(!properties.time) {
			properties.time = 1000;
		}


		interactStarlingTouch(TouchPhase.BEGAN, component, properties);
		setTimeout(interactStarlingTouch, properties.time, TouchPhase.ENDED, component, properties);
		trace("Timeout: " + properties.time);
	}

	static public function touchClick(component:IDisplayObject, properties:Object=null):void
	{
		interactStarlingTouch(TouchPhase.BEGAN, component, properties);
		interactStarlingTouch(TouchPhase.ENDED, component, properties);
	}

	private static function interactStarlingTouch(eventPhase:String, component:IDisplayObject, properties:Object):void
	{
		trace("Touch Click");
		// Default properties to an empty hash
		if(properties == null) {
			properties = {};
		}



		// Component must be mouse enabled
		if(!component.mouseEnabled) {
			throw new MelomelError("Mouse events are not enabled on component");
		}

		// Center the click
		if(!properties.localX) {
			properties.localX = Math.floor(component.width/2);
		}
		if(!properties.localY) {
			properties.localY = Math.floor(component.height/2);
		}

		// convert the coord to global
		var local:Point = new Point(properties.localX, properties.localY);
		var global:Point = new Point();
		component.base.localToGlobal(local,global);

		var touch:Touch = new Touch(0);
		touch.phase = eventPhase;
		touch.globalX = global.x;
		touch.globalY = global.y;
		touch.height = 1;
		touch.pressure = 1;
		touch.tapCount = 1;
		if(!properties.target) {
			touch.target = component.base as DisplayObject;
		} else {
			touch.target = properties.target as DisplayObject;
		}
		touch.width = 1;

		var touches:Vector.<Touch> = new Vector.<Touch>();
		touches[0] = touch;
		var event:TouchEvent = new TouchEvent(TouchEvent.TOUCH, touches);
		var qd:Quad = new Quad(5, 5, 0xff0000);
		qd.x = global.x;
		qd.y = global.y;
		qd.touchable = false;
		_pointArray.push(qd);
		if(_removeTimer.running == false) {
			_removeTimer.addEventListener(TimerEvent.TIMER, removeTouchPoint);
			_removeTimer.start();
		}
		Starling.current.stage.addChild(qd);

		//var stage:Stage = findStage(component);
		//stage.dispatchEvent(event)
		//Starling.current.dispatchEvent(event);
		if(!properties.target) {
			(component.base as DisplayObject).dispatchEvent(event);
		} else {
			(properties.target as DisplayObject).dispatchEvent(event);
		}

	}

	/**
	 *	Imitates a gestouch tap on a component.
	 *
	 *	@param component   The component to click.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	public static function touch(component:IDisplayObject,
								 properties:Object=null):void {

		interactTouch(new MouseEvent(MouseEvent.MOUSE_DOWN), component, properties);
		interactTouch(new MouseEvent(MouseEvent.MOUSE_UP), component, properties);
	}

	/**
	 *	Imitates a double click on a component.
	 *
	 *	@param component   The component to double click.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function doubleClick(component:IDisplayObject,
									   properties:Object=null):void
	{
		// Component must be mouse enabled
		if(!component.doubleClickEnabled) {
			throw new MelomelError("Double clicking is not enabled on component");
		}

		click(component, properties);
		click(component, properties);
		interact(new MouseEvent(MouseEvent.DOUBLE_CLICK), component, properties)
	}

	/**
	 *	Imitates the mouse button clicking down on a component.
	 *
	 *	@param component   The component to click down.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function mouseDown(component:IDisplayObject,
									 properties:Object=null):void
	{
		interact(new MouseEvent(MouseEvent.MOUSE_DOWN), component, properties)
	}

	/**
	 *	Imitates the mouse button being released on a component.
	 *
	 *	@param component   The component to click up.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function mouseUp(component:IDisplayObject,
								   properties:Object=null):void
	{
		interact(new MouseEvent(MouseEvent.MOUSE_UP), component, properties)
	}

	/**
	 *	Imitates the a keyboard key being pressed down on a component.
	 *
	 *	@param component   The component that should receive the event.
	 *	@param char        The keyboard character being pressed.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function keyDown(component:IDisplayObject, char:Object,
								   properties:Object=null):void
	{
		if(properties == null) {
			properties = {};
		}
		properties["char"] = char;
		interactKeyboard(new KeyboardEvent(KeyboardEvent.KEY_DOWN), component, properties)
	}

	/**
	 *	Imitates the a keyboard key being released on a component.
	 *
	 *	@param component   The component that should receive the event.
	 *	@param char        The keyboard character being pressed.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function keyUp(component:IDisplayObject, char:Object,
								 properties:Object=null):void
	{
		if(properties == null) {
			properties = {};
		}
		properties["char"] = char;
		interactKeyboard(new KeyboardEvent(KeyboardEvent.KEY_UP), component, properties)
	}

	/**
	 *	Imitates the a keyboard key being pressed and released on a component.
	 *
	 *	@param component   The component that should receive the event.
	 *	@param char        The keyboard character being pressed.
	 *	@param properties  A hash of properties to set on the event object.
	 */
	static public function keyPress(component:IDisplayObject, char:Object,
									properties:Object=null):void
	{
		if(properties == null) {
			properties = {};
		}
		properties["char"] = char;

		keyDown(component, char, properties);
		keyUp(component, char, properties);
	}


	//---------------------------------
	//	Data Control Interaction
	//---------------------------------

	/**
	 *	Generates a list of labels from a data control.
	 *
	 *	@param component  A data control or column which has an itemToLabel()
	 *	                  method.
	 *	@param data       The dataset to generate labels from.
	 *
	 *	@return           A list of labels generated by the component.
	 */
	static public function itemsToLabels(component:Object, data:Object):Array
	{
		// Use source property if this is an ArrayList.
		if(Type.typeOf(data, "mx.collections.ArrayList")) {
			data = data.source;
		}

		// Loop over collection and generate labels
		var labels:Array = [];

		for each(var item:Object in data) {
			labels.push(component.itemToLabel(item));
		}

		return labels;
	}


	//---------------------------------
	//	Tree access
	//---------------------------------

	/**
	 *	Retrieves an item from a Tree control by label.
	 *
	 *	@param tree   A Flex tree.
	 *	@param label  The label of the item in the tree.
	 *
	 *	@return       The item in the tree that matches the label.
	 */
	static public function findTreeItemByLabel(tree:Object, label:String):Object
	{
		if(tree == null || tree.dataProvider == null) {
			return null;
		}

		return _findTreeItemByLabel(tree, label, tree.dataProvider.getItemAt(0));
	}

	static private function _findTreeItemByLabel(tree:Object, label:String, data:Object):Object
	{
		// If the label matches then return it
		var labelString:String = tree.itemToLabel(data);
		if(labelString == label) {
			return data;
		}

		// Otherwise search children
		var children:Object = tree.dataDescriptor.getChildren(data);
		if(children) {
			for each(var child:Object in children) {
				var ret:Object = _findTreeItemByLabel(tree, label, child);
				if(ret) {
					return ret;
				}
			}
		}

		// Nothing found.
		return null;
	}


	//--------------------------------------------------------------------------
	//
	//	Constructor
	//
	//--------------------------------------------------------------------------

	/**
	 *	Constructor.
	 */
	public function UI()
	{
		throw new MelomelError("Cannot instantiate the UI class");
	}

	static private function removeTouchPoint(event:TimerEvent):void {
		if(_pointArray.length > 0) {
			Starling.current.stage.removeChild(_pointArray[0]);
			_pointArray.splice(0, 1);
		} else {
			_removeTimer.stop();
			_removeTimer.removeEventListener(TimerEvent.TIMER, removeTouchPoint);
		}
	}
}
}