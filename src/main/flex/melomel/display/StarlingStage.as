/**
 * Created by njackson on 06/12/2013.
 */
package melomel.display {
import flash.events.Event;

import melomel.display.StarlingDisplayObject;
import melomel.display.StarlingDisplayObjectContainer;

import starling.display.Stage;

public class StarlingStage extends StarlingDisplayObjectContainer implements IStage {

    public function StarlingStage(stage:Stage) {
        super(stage);
    }

    public function hasEventListener(type:String):Boolean {
        return false;
    }

    public function willTrigger(type:String):Boolean {
        return false;
    }

    //FROM IDisplayObject

    public override function get parent():IDisplayObject {
        return null; // stage has no parent
    }

    public function removeEventListener(event:String, func:Function):* {
    }

    public function addEventListener(event:String, func:Function):* {
    }
}
}

