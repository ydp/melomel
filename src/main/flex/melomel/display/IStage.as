/**
 * Created by njackson on 06/12/2013.
 */
package melomel.display {

import flash.events.IEventDispatcher;

    // IStage is an base level abstraction for flash.display.stage and starling.display.stage
    public interface IStage extends IDisplayObjectContainer, IDisplayObject {
        function removeEventListener(event:String, func:Function):*;
        function addEventListener(event:String, func:Function):*;
    }

}
