/**
 * Created by njackson on 09/12/2013.
 */
package melomel.display {
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.InteractiveObject;
import flash.events.Event;

import melomel.core.Type;

import mx.core.UIComponent;

public class FlashDisplayObject implements IDisplayObject {

    internal var _this:DisplayObject;

    public function FlashDisplayObject(displayObject:DisplayObject) {
        _this = displayObject;
    }

    public function get parent():IDisplayObject {
        if(_this.parent is DisplayObjectContainer)
            return new FlashDisplayObjectContainer(_this.parent);
        else if(_this.parent is DisplayObject)
            return new FlashDisplayObject(_this.parent);
        else
            return null;
    }

    public function get base():Object {
        return _this;
    }

    public function get enabled():Boolean {
        var component:mx.core.UIComponent = _this as mx.core.UIComponent;
        return (component == null) ? true: component.enabled;
    }

    public function get mouseEnabled():Boolean {
        var obj:InteractiveObject = _this as InteractiveObject;
        return (obj == null) ? false : obj.mouseEnabled;
    }

    public function get doubleClickEnabled():Boolean {
        var obj:InteractiveObject = _this as InteractiveObject;
        return (obj == null) ? false : obj.doubleClickEnabled;
    }

    public function get width():int {
        return _this.width;
    }

    public function get height():int {
        return _this.height;
    }

    public function get visible():Boolean {
        return _this.visible;
    }

    public function hasOwnProperty(property:String):Boolean {
        return _this.hasOwnProperty(property);
    }

    public function getValueForProperty(property:String):Object {
        return _this[property];
    }

    public function dispatchEvent(event:Event):void {
        _this.dispatchEvent(event);
    }

}
}
