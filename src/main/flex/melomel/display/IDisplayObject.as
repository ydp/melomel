/**
 * Created by njackson on 06/12/2013.
 */
package melomel.display {
import flash.events.Event;

public interface IDisplayObject {

    function get base():Object; // the raw object
    function get parent():IDisplayObject;
    function get visible():Boolean;
    function get enabled():Boolean;
    function get mouseEnabled():Boolean;
    function get doubleClickEnabled():Boolean;
    function get width():int;
    function get height():int;


    function hasOwnProperty(property:String):Boolean;
    function getValueForProperty(property:String):Object;
    function dispatchEvent(event:Event):void;

}
}
