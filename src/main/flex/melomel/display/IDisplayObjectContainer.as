/**
 * Created by njackson on 10/12/2013.
 */
package melomel.display {
public interface IDisplayObjectContainer extends IDisplayObject{

    function get numChildren():int;

    function getChildAt(index:int):IDisplayObject;

}
}
