/**
 * Created by njackson on 06/12/2013.
 */
package melomel.display {
import flash.display.Stage;
import flash.events.Event;

public class FlashStage extends FlashDisplayObjectContainer implements IStage {

    public function FlashStage(stage:Stage ) {
        super(stage);
    }

    public function removeEventListener(event:String, func:Function):* {
        _this.removeEventListener(event,func);
    }

    public function addEventListener(event:String, func:Function):* {
        _this.addEventListener(event,func);
    }
}
}
