/**
 * Created by njackson on 11/12/2013.
 */
package melomel.display {
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;

import melomel.core.Type;

public class FlashDisplayObjectContainer extends FlashDisplayObject implements IDisplayObjectContainer {

    private var rawChildrenClasses:Array = [Type.getClass("mx.core.Container"),Type.getClass("mx.containers.TabNavigator")];

    public function FlashDisplayObjectContainer(displayObject:DisplayObjectContainer) {
        super(displayObject);
    }

    private function hasRawChildren():Boolean {
        for each(var rawChildrenClass:Class in rawChildrenClasses) {
            if(_this is rawChildrenClass) {
                return true;
            }
        }

        return false;
    }

    public function get numChildren():int {
        if(hasRawChildren())
            return (_this as Object).rawChildren.numChildren;
        else
            return (_this as DisplayObjectContainer).numChildren;
    }

    public function getChildAt(index:int):IDisplayObject {

        var child:DisplayObject;

        if(hasRawChildren())
            child = (_this as Object).rawChildren.getChildAt(index);
        else
            child = (_this as DisplayObjectContainer).getChildAt(index)

        if(child is DisplayObjectContainer)
            return new FlashDisplayObjectContainer(child as DisplayObjectContainer);
        else
            return new FlashDisplayObject(child);

    }

}
}
