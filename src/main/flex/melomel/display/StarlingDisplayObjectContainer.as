/**
 * Created by njackson on 11/12/2013.
 */
package melomel.display {
import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

public class StarlingDisplayObjectContainer extends StarlingDisplayObject implements IDisplayObjectContainer {

    public function StarlingDisplayObjectContainer(displayObject:DisplayObject) {
        super(displayObject);

    }

    public function get numChildren():int {
        return (_this as DisplayObjectContainer).numChildren;
    }

    public function getChildAt(index:int):IDisplayObject {

        var child:DisplayObject = (_this as DisplayObjectContainer).getChildAt(index);

        if(child is DisplayObjectContainer)
            return new StarlingDisplayObjectContainer(child as DisplayObjectContainer);
        else
            return new StarlingDisplayObject(child);

    }
}
}
