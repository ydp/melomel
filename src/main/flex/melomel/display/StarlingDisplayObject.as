/**
 * Created by njackson on 09/12/2013.
 */
package melomel.display {
import flash.events.Event;

import melomel.display.StarlingDisplayObject;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.events.Event;

public class StarlingDisplayObject implements IDisplayObject {

    internal var _this:DisplayObject

    public function StarlingDisplayObject(displayObject:DisplayObject) {
        _this = displayObject;
    }

    public function get parent():IDisplayObject {
        if(_this.parent is DisplayObjectContainer)
            return new StarlingDisplayObjectContainer(_this.parent);
        else if(_this.parent is DisplayObject)
            return new StarlingDisplayObject(_this.parent);
        else
            return null;
    }

    public function get visible():Boolean {
        return _this.visible;
    }

    public function hasOwnProperty(property:String):Boolean {
        return _this.hasOwnProperty(property);
    }

    public function getValueForProperty(property:String):Object {
        return _this[property];
    }

    public function get base():Object {
        return _this;
    }

    public function get enabled():Boolean
    {
        if(hasOwnProperty("enabled"))
            return getValueForProperty("enabled")
        else
            return _this.visible;
    }

    public function get mouseEnabled():Boolean
    {
        return _this.touchable;
    }

    public function get doubleClickEnabled():Boolean
    {
        return _this.touchable;
    }

    public function get width():int
    {
        return _this.width;
    }

    public function get height():int
    {
        return _this.height;
    }

    public function dispatchEvent(event:flash.events.Event):void
    {
        _this.dispatchEvent(new starling.events.Event(starling.events.Event.TRIGGERED,true,null));
    }
}
}
